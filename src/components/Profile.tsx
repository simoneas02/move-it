import { useContext } from 'react'
import { ChallengesContext } from '../context/ChallengesContext'
import styles from '../styles/components/Profile.module.css'

const Profile = () => {
  const { level } = useContext(ChallengesContext)

  return (
    <div className={styles.profileContainer}>
      <img
        className={styles.profileAvatar}
        src="https://github.com/simoneas02.png"
        alt="Simone github profile"
      />

      <div className={styles.profileInfo}>
        <p className={styles.profileHighlightText}>Simone Amorim</p>
        <div className={styles.profileInfoLevel}>
          <img src="icons/level.svg" alt="arrow top" />

          <p className={styles.profileText}>Level {level}</p>
        </div>
      </div>
    </div>
  )
}

export default Profile
