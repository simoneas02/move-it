import { useContext } from 'react'
import { ChallengesContext } from '../context/ChallengesContext'
import styles from '../styles/components/CompleteChallenges.module.css'

const CompletedChallenges = () => {
  const { challengesCompleted } = useContext(ChallengesContext)

  return (
    <div className={styles.completedChallengesContainer}>
      <h2 className={styles.completedChallengesTitle}>Desafios completos</h2>
      <span className={styles.completedChallengesCounter}>
        {challengesCompleted}
      </span>
    </div>
  )
}

export default CompletedChallenges
