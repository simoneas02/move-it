import { useContext } from 'react'
import { ChallengesContext } from '../context/ChallengesContext'
import styles from '../styles/components/LevelUpModal.module.css'

const LevelUpModal = () => {
  const { level, closeLevelUpModal } = useContext(ChallengesContext)
  return (
    <div className={styles.levelUpModaloverlay}>
      <div className={styles.levelUpModalContainer}>
        <h2 className={styles.levelUpModalLevel}>{level}</h2>

        <span className={styles.levelUpModalHighLight}>Parabéns</span>
        <p className={styles.levelUpModalText}>Você alcançou um novo level.</p>

        <button
          className={styles.levelUpModalBtn}
          type="button"
          onClick={closeLevelUpModal}
        >
          <img
            className={styles.levelUpModalIcon}
            src="/icons/close.svg"
            alt="Fechar modal"
          />
        </button>
      </div>
    </div>
  )
}

export default LevelUpModal
