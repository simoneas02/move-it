import { useContext } from 'react'
import { ChallengesContext } from '../context/ChallengesContext'
import { CountdownContext } from '../context/CountdownContext'
import styles from '../styles/components/ChallengeBox.module.css'

const ChallengeBox = () => {
  const { activeChallenge, resetChallenge, completeChallenge } = useContext(
    ChallengesContext
  )
  const { resetCountdown } = useContext(CountdownContext)

  const handleChallengeSucceeded = () => {
    completeChallenge()
    resetCountdown()
  }

  const handleChallengeFailed = () => {
    resetChallenge()
    resetCountdown()
  }

  return (
    <div className={styles.challengeBoxContainer}>
      {activeChallenge ? (
        <div className={styles.challengeActive}>
          <h2 className={styles.challengeActiveTitle}>
            Ganhe {activeChallenge.amount} xp
          </h2>

          <div className={styles.challengeActiveInfo}>
            <img
              className={styles.challengeActiveInfoIcon}
              src={`icons/${activeChallenge.type}.svg`}
              alt="Body"
            />
            <h3 className={styles.challengeActiveInfoTitle}>Novo desafio</h3>
            <p className={styles.challengeActiveInfoText}>
              {activeChallenge.description}
            </p>
          </div>
          <div className={styles.challengeActiveInfoBtnGroup}>
            <button
              className={`${styles.challengeActiveInfoBtn} ${styles.challengeActiveInfoBtnFailed}`}
              type="button"
              onClick={handleChallengeFailed}
            >
              Falhei
            </button>
            <button
              className={`${styles.challengeActiveInfoBtn} ${styles.challengeActiveInfoBtnSucceeded}`}
              type="button"
              onClick={handleChallengeSucceeded}
            >
              Completei
            </button>
          </div>
        </div>
      ) : (
        <div className={styles.challengeNotActive}>
          <h2 className={styles.challengeTitle}>
            Finalize um ciclo para receber um desafio
          </h2>

          <div className={styles.challengeinfo}>
            <img
              className={styles.challengeInfoIcon}
              src="icons/level-up.svg"
              alt="Level Up"
            />
            <p className={styles.challengeInfoText}>
              Avance de level completando desafios
            </p>
          </div>
        </div>
      )}
    </div>
  )
}

export default ChallengeBox
