import { useContext } from 'react'
import { ChallengesContext } from '../context/ChallengesContext'
import styles from '../styles/components/ExperienceBar.module.css'

const ExperienceBar = () => {
  const { currentExperience, experienceToNextLevel } = useContext(
    ChallengesContext
  )

  const percentToNextLevel =
    Math.round(currentExperience * 100) / experienceToNextLevel

  return (
    <header className={styles.experienceBar}>
      <span className={styles.experienceBarText}>0 xp</span>

      <div className={styles.experienceBarFill}>
        <div
          className={styles.experienceBarFilled}
          style={{ width: `${percentToNextLevel}%` }}
        />
        <span
          className={styles.experienceBarCurrentFilled}
          style={{ left: `${percentToNextLevel}%` }}
        >
          {currentExperience} xp
        </span>
      </div>

      <span className={styles.experienceBarText}>
        {experienceToNextLevel} xp
      </span>
    </header>
  )
}

export default ExperienceBar
